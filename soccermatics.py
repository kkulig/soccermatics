#!/usr/bin/env python
# coding: utf-8

# todo:
# 
# probability of goal in this minute = mean number of goals for each game / 90
# 
# test of  probability - distribution Poison 

# In[4]:


import pandas as pd
import matplotlib.pyplot as plt


# In[2]:


df = pd.read_csv('data/17-18.csv')
df.head()


# mean number of goals for each game
# 
# FTHG - Full Time Home Team Goals
# 
# FTAG - Full Time Away Team Goals

# In[3]:


mean_goals_for_game = (df["FTHG"].sum() + df["FTAG"].sum()) / len(df["FTHG"])
mean_goals_for_game


# histogram shooting goals in season 2017/2018, number of games / number of goals

# In[11]:


results_sum_goals = [0 for x in range(11)]
for i in range(len(df)):
    FTHG = df.iloc[i]['FTHG']
    FTAG = df.iloc[i]['FTAG']
    results_sum_goals[FTHG + FTAG] += 1
results_sum_goals


# In[13]:


plt.bar([x for x in range(11)], results_sum_goals)
plt.xlabel('number of games')
plt.ylabel('number of goals')
plt.show()

